package com.atlassian.bamboo.plugins.tap;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.build.test.TestCollectionResultBuilder;
import com.atlassian.bamboo.build.test.TestReportProvider;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.resultsummary.tests.TestCaseResultErrorImpl;
import com.atlassian.bamboo.resultsummary.tests.TestState;

import org.jetbrains.annotations.NotNull;
import org.tap4j.consumer.TapConsumer;
import org.tap4j.consumer.TapConsumerFactory;
import org.tap4j.model.Comment;
import org.tap4j.model.Directive;
import org.tap4j.model.TestResult;
import org.tap4j.model.TestSet;
import org.tap4j.util.DirectiveValues;
import org.tap4j.util.StatusValues;

public class TapTestReportProvider implements TestReportProvider {
    private final File logFile;
    private final boolean failFailedTodoResults;

    Collection<TestResults> failedTests = new ArrayList<TestResults>();
    Collection<TestResults> successfulTests = new ArrayList<TestResults>();
    Collection<TestResults> skippedTests = new ArrayList<TestResults>();

    public TapTestReportProvider(File logFile) {
        this(logFile, false);
    }

    public TapTestReportProvider(File logFile, boolean failFailedTodoResults) {
        this.logFile = logFile;
        this.failFailedTodoResults = failFailedTodoResults;
    }

    @NotNull
    @Override
    public TestCollectionResult getTestCollectionResult() {
        final TapConsumer tapConsumer = TapConsumerFactory.makeTap13Consumer();

        processTestSet(tapConsumer.load(logFile));

        return new TestCollectionResultBuilder()
                .addFailedTestResults(failedTests)
                .addSkippedTestResults(skippedTests)
                .addSuccessfulTestResults(successfulTests).build();
    }

    protected void processTestSet(final TestSet testSet) {
        for (TestResult tapTestResult : testSet.getTestResults()) {
            final Directive directive = tapTestResult.getDirective();
            final StatusValues status = tapTestResult.getStatus();
            final String description = tapTestResult.getDescription();

            TestResults bambooTestResult = new TestResults(description, description, null);

            if (directive != null && (directive.getDirectiveValue().equals(DirectiveValues.SKIP) || (status.equals(StatusValues.NOT_OK) && !failFailedTodoResults))) {
                bambooTestResult.setState(TestState.SKIPPED);
                skippedTests.add(bambooTestResult);
            } else {
                switch (status) {
                    case OK:
                        bambooTestResult.setState(TestState.SUCCESS);
                        successfulTests.add(bambooTestResult);
                        break;
                    case NOT_OK:
                        for (final Comment comment : tapTestResult.getComments()) {
                            bambooTestResult.addError(new TestCaseResultErrorImpl(comment.getText()));
                        }
                        bambooTestResult.setState(TestState.FAILED);
                        failedTests.add(bambooTestResult);
                        break;
                }
            }
        }
    }
}
