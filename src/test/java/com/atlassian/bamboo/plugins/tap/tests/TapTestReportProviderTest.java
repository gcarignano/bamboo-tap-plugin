package com.atlassian.bamboo.plugins.tap.tests;

import java.io.File;
import java.net.URISyntaxException;

import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.plugins.tap.TapTestReportProvider;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

public class TapTestReportProviderTest {
    File successfulLog;
    File mixedLog;
    File directivesLog;

    @Before
    public void setupLogFiles() throws URISyntaxException {
        successfulLog = new File(getClass().getResource("/com/atlassian/bamboo/plugins/tap/tests/successlog.txt").toURI());
        mixedLog = new File(getClass().getResource("/com/atlassian/bamboo/plugins/tap/tests/mixedlog.txt").toURI());
        directivesLog = new File(getClass().getResource("/com/atlassian/bamboo/plugins/tap/tests/directiveslog.txt").toURI());
    }

    @Test
    public void testParsesSuccessfulResults() {
        final TapTestReportProvider provider = new TapTestReportProvider(successfulLog);
        final TestCollectionResult testCollectionResult = provider.getTestCollectionResult();
        Assert.assertEquals(0, testCollectionResult.getFailedTestResults().size());
        Assert.assertEquals(5, testCollectionResult.getSuccessfulTestResults().size());
        Assert.assertEquals(0, testCollectionResult.getSkippedTestResults().size());
    }

    @Test
    public void testParsesMixedResults() {
        final TapTestReportProvider provider = new TapTestReportProvider(mixedLog);
        final TestCollectionResult testCollectionResult = provider.getTestCollectionResult();
        Assert.assertEquals(2, testCollectionResult.getFailedTestResults().size());
        Assert.assertEquals(2, testCollectionResult.getSuccessfulTestResults().size());
        Assert.assertEquals(1, testCollectionResult.getSkippedTestResults().size());
    }

    @Test
    public void testParsesDirectives() {
        final TapTestReportProvider provider = new TapTestReportProvider(directivesLog);
        final TestCollectionResult testCollectionResult = provider.getTestCollectionResult();
        Assert.assertEquals(2, testCollectionResult.getFailedTestResults().size());
        Assert.assertEquals(2, testCollectionResult.getSuccessfulTestResults().size());
        Assert.assertEquals(3, testCollectionResult.getSkippedTestResults().size());

        final TapTestReportProvider provider2 = new TapTestReportProvider(directivesLog, true);
        final TestCollectionResult testCollectionResult2 = provider2.getTestCollectionResult();
        Assert.assertEquals(3, testCollectionResult2.getFailedTestResults().size());
        Assert.assertEquals(2, testCollectionResult2.getSuccessfulTestResults().size());
        Assert.assertEquals(2, testCollectionResult2.getSkippedTestResults().size());
    }
}
